﻿#pragma strict
var startPos:Vector3;
var endPos:Vector3;
var speed:float;
var dir:Vector3;
function Start () {
startPos = transform.position;
endPos = startPos+dir;
transform.position+=dir*speed;
}

function Update () {
if(transform.position == endPos||transform.position == startPos) dir*=-1;
transform.position+=dir*speed;
}

function OnTriggerEnter(c:Collider){
	c.gameObject.transform.SetParent(gameObject.transform);
}
