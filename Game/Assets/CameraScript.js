﻿#pragma strict
var target:GameObject;
var rotateSpeed:float = 5;
var offset:Vector3;
function Start () {
	offset = target.transform.position - transform.position;
}

function Update () {
	var horizontal = Input.GetAxis("Mouse X") * rotateSpeed;
	target.transform.Rotate(0, horizontal, 0);
	var desiredAngle = target.transform.eulerAngles.y;
	var rotation = Quaternion.Euler(0, desiredAngle, 0);
	transform.position = target.transform.position - (rotation * offset);
	transform.LookAt(target.transform);	
}